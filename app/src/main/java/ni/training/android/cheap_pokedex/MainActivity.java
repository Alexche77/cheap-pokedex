package ni.training.android.cheap_pokedex;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import ni.training.android.cheap_pokedex.Fragmentos.Perfil;
import ni.training.android.cheap_pokedex.Fragmentos.PokemonFragment;
import ni.training.android.cheap_pokedex.Fragmentos.WikiFragment;
import ni.training.android.cheap_pokedex.Models.PokemonRespuesta;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, Perfil.OnListFragmentInteractionListener {


    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private MenuItem prevMenuItem;
    private AppBarLayout appBarLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        viewPager = (ViewPager) findViewById(R.id.pager);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        configurarViewPager();
        /*
        * Creando una instancia de retrofit
        * para traer los pokemones y mostrarlos en la lista principal
        * */



        bottomNavigationView.setSelectedItemId(R.id.navigation_discover);


    }

    private void configurarViewPager() {
        AdaptadorViewPager adapter = new AdaptadorViewPager(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                appBarLayout.setExpanded(true);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_discover:
                viewPager.setCurrentItem(0);
                break;
            case R.id.navigation_wiki:
                viewPager.setCurrentItem(1);
                break;
            case R.id.navigation_profile:
                viewPager.setCurrentItem(2);
                break;
        }
        appBarLayout.setExpanded(true);
        return true;
    }



    @Override
    public void onListFragmentInteraction(PokemonRespuesta.Pokemon item) {
        Toast.makeText(MainActivity.this, "Abrir el detalle de este pokemon: " + item.getName(), Toast.LENGTH_SHORT).show();
    }

    private class AdaptadorViewPager extends FragmentPagerAdapter {

        AdaptadorViewPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f;
            switch (position) {
                case 0:
                    f = PokemonFragment.newInstance();
                    break;
                case 1:

                    f = WikiFragment.newInstance();
                    break;
                case 2:
                    f = Perfil.newInstance();
                    break;
                default:
                    f = PokemonFragment.newInstance();
                    break;


            }
            return f;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
