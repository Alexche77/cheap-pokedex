package ni.training.android.cheap_pokedex.PokeApi;

import ni.training.android.cheap_pokedex.Models.PokemonRespuesta;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alvaro on 3/9/2017.
 *
 * Interfaz de retrofit que se encarga de disponer
 * los metodos de comunicacion con la pokeapi.
 */

public interface PokeApiService {

    @GET("pokemon")
    Call<PokemonRespuesta> obtenerListaPokemon(@Query("offset") int offset);

    @GET("pokemon/{id}/")
    Call<PokemonRespuesta.Pokemon> obtenerPokemon(@Path("id") int pokemonId);
}
