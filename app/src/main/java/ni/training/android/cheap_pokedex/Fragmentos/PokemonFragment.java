package ni.training.android.cheap_pokedex.Fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.db.rossdeckview.FlingChief;
import com.db.rossdeckview.FlingChiefListener;
import com.db.rossdeckview.RossDeckView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;

import ni.training.android.cheap_pokedex.Adaptadores.DeckAdapter;
import ni.training.android.cheap_pokedex.Models.PokemonRespuesta;
import ni.training.android.cheap_pokedex.PokeApi.PokeApiService;
import ni.training.android.cheap_pokedex.R;
import ni.training.android.cheap_pokedex.Tools.Constantes;
import ni.training.android.cheap_pokedex.Tools.MySQLiteOpenHelper;
import ni.training.android.cheap_pokedex.Tools.RandomIdGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokemonFragment extends Fragment {


    private static Retrofit retrofit;
    private static DeckAdapter mAdapter;
    private LinkedList<PokemonRespuesta.Pokemon> pokemones;
    private CardStackView mDeckLayout;

    public PokemonFragment() {
    }

    public static PokemonFragment newInstance() {

        return new PokemonFragment();
    }

    private void obtenerPokemon(int id) {
        Log.d("POKE-CARDS","Buscando pokemon con el id "+id);
//        Utilizamos nuestra interfaz creandola con retrofit
        PokeApiService apiService = retrofit.create(PokeApiService.class);
        Call<PokemonRespuesta.Pokemon> pokemonRespuestaCall = apiService.obtenerPokemon(id);
        pokemonRespuestaCall.enqueue(new Callback<PokemonRespuesta.Pokemon>() {
            @Override
            public void onResponse(@NonNull Call<PokemonRespuesta.Pokemon> call, @NonNull Response<PokemonRespuesta.Pokemon> response) {
                PokemonRespuesta.Pokemon respuestaPokemon = response.body();

                if (respuestaPokemon != null) {
                    Log.d("RESPUESTA:","EXITO->Pokemon encontrado: "+respuestaPokemon.getName());
                    pokemones.addLast(respuestaPokemon);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PokemonRespuesta.Pokemon> call, Throwable t) {
                Log.e("RESPUESTA:","ERROR");
            }
        });
        Log.d("RESPUESTA","Hay "+pokemones.size()+" pokemones en la lista");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            if (!savedInstanceState.isEmpty()){
                Log.d("JSON","Los pokemones obtenidos son"+savedInstanceState.getString("pokemonesJSON"));
                if (pokemones==null){
                    Log.d("JSON","No hay nada en la lista");
                    Log.d("JSON","Obteniendo la string y convirtiendola auna lista de objetos");
                    Gson g = new Gson();
                    pokemones = g.fromJson(savedInstanceState.getString("pokemonesJSON"),new TypeToken<ArrayList<PokemonRespuesta.Pokemon>>(){}.getType());
                    Log.d("JSON","Lista de pokemones:"+pokemones.toString());
                }
            }else{
                Log.d("JSON","No hay elementos: "+savedInstanceState.size());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discover_pokemons, container, false);
        retrofit = new Retrofit.Builder().baseUrl(Constantes.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        if(pokemones==null){
            pokemones = new LinkedList<>();
        }
        mDeckLayout = (CardStackView) view.findViewById(R.id.decklayout);
        mDeckLayout.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {

            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                if (direction == SwipeDirection.Top){
                    //Este metodo se deberia ejecutar en segundo plano
                    agregarPokeAFavs(pokemones.iterator().next());
                }
              quitarPrimerPokemon();
            }

            @Override
            public void onCardReversed() {

            }

            @Override
            public void onCardMovedToOrigin() {

            }

            @Override
            public void onCardClicked(int index) {

            }
        });
        mAdapter = new DeckAdapter(getContext(),R.layout.card_pokemon,pokemones);

        obtenerPokemon(RandomIdGenerator.showRandomInteger(1,150));
        obtenerPokemon(RandomIdGenerator.showRandomInteger(1,150));
        obtenerPokemon(RandomIdGenerator.showRandomInteger(1,150));

        mDeckLayout.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }



    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Gson g = new Gson();
//        Obteniendo info sobre el tipo de objeto que se va a guardar
        Type pokeGuardar = new TypeToken<ArrayList<PokemonRespuesta.Pokemon>>(){}.getType();
        String jsonPokemones = g.toJson(pokemones,pokeGuardar);
        Log.d("JSON","Guardar la cadena:"+jsonPokemones);
        outState.putString("pokemonesJSON",jsonPokemones);


    }




    private void quitarPrimerPokemon() {
        LinkedList<PokemonRespuesta.Pokemon> pokemiones = extraerPokemonesRestantes();
        if (pokemiones.isEmpty()) {
            return;
        }

        pokemiones.removeFirst();
        mAdapter.clear();
        mAdapter.addAll(pokemiones);
        mAdapter.notifyDataSetChanged();
    }
    private void agregarPokemon() {
        LinkedList<PokemonRespuesta.Pokemon> pokemiones = extraerPokemonesRestantes();
        obtenerPokemon(RandomIdGenerator.showRandomInteger(1,150));
        mAdapter.clear();
        mAdapter.addAll(pokemiones);
        mAdapter.notifyDataSetChanged();
    }

    private LinkedList<PokemonRespuesta.Pokemon> extraerPokemonesRestantes() {
        LinkedList<PokemonRespuesta.Pokemon> pokemiones = new LinkedList<>();
        for (int i = mDeckLayout.getTopIndex(); i < mAdapter.getCount(); i++) {
            pokemiones.add(mAdapter.getItem(i));
        }
        return pokemiones;
    }

    private void agregarPokeAFavs(PokemonRespuesta.Pokemon item) {
        Toast.makeText(getContext(), "Guardado en favoritos: " + item.getName(), Toast.LENGTH_SHORT).show();

        MySQLiteOpenHelper helper = new MySQLiteOpenHelper(getContext());

        helper.agregarPokemon(item);
    }

}
