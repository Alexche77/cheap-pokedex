package ni.training.android.cheap_pokedex.Tools;
import android.util.Log;

import java.util.Random;


/**
 * Created by Alvaro on 4/9/2017.
 */

public class RandomIdGenerator {

    public static int showRandomInteger(int aStart, int aEnd){
        log("Generating random integers");

        Random aRandom = new Random();

        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = (long)aEnd - (long)aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long)(range * aRandom.nextDouble());
        int randomNumber =  (int)(fraction + aStart);
        log("Generated : " + randomNumber);
        return randomNumber;
    }

    private static void log(String aMessage){
        Log.d("RANDOM-GEN","Generado!"+aMessage);
    }
}
