package ni.training.android.cheap_pokedex.Tools;

/**
 * Created by Alvaro on 3/9/2017.
 */

public class Constantes {
    public static final String BASE_URL = "https://pokeapi.co/api/v2/";
    public static final String URL_IMG_POKEMONES_GRANDES = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other-sprites/official-artwork/";

    public class ENDPOINTS{
        public static final String POKEMONES = "pokemon";

        public static final String POKE_FORMS = "pokemon-form";
    }
}

