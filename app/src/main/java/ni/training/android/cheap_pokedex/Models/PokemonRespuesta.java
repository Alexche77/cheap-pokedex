package ni.training.android.cheap_pokedex.Models;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by Alvaro on 3/9/2017.
 *
 * POJO para la respuesta que se recibe desde el endpoin
 * "https://pokeapi.co/api/v2/pokemon/"
 *
 * Utilizado en la pantalla principal, donde salen los pokemones
 */

public class PokemonRespuesta {
    @SerializedName("results")
    private ArrayList<Pokemon> results;

    public ArrayList<Pokemon> getResults() {
        return results;
    }

    public void setResults(ArrayList<Pokemon> results) {
        this.results = results;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPrevious_url() {
        return previous_url;
    }

    public void setPrevious_url(String previous_url) {
        this.previous_url = previous_url;
    }

    public String getNext_url() {
        return next_url;
    }

    public void setNext_url(String next_url) {
        this.next_url = next_url;
    }

    @SerializedName("count")
    private int count;
    @SerializedName("previous")
    private String previous_url;
    @SerializedName("next")
    private String next_url;

    public static class Pokemon{
        public Pokemon() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @SerializedName("name")
        private String name;
        @SerializedName("url")
        private String url;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @SerializedName("id")
        private int id;
    }
}
