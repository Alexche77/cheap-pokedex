package ni.training.android.cheap_pokedex.Tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.text.SimpleDateFormat;

import ni.training.android.cheap_pokedex.Models.PokemonRespuesta;

/**
 * Created by Alvaro on 2/9/2017.
 */

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    //Variables
    private static final String DATABASE_NAME="Pokemons";

    private static final String TABLE_FAVS="pokefavs";


    public MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MyDB_TABLE = "CREATE TABLE IF NOT EXISTS "+TABLE_FAVS+" (nombre INTEGER PRIMARY KEY, pokemon_id INTEGER,nombre_pokemon VARCHAR, capturado DATETIME DEFAULT CURRENT_TIMESTAMP)";
        db.execSQL(CREATE_MyDB_TABLE);
    }

    public void agregarPokemon(PokemonRespuesta.Pokemon item){
        Log.d("POKE_FAV","AGREGANDO POKEMON A FAVS");
        SQLiteDatabase db=this.getWritableDatabase();
//        INSERT INTO +TABLE_FAVS+ values (?,?,?)
        ContentValues values=new ContentValues();
        values.put("pokemon_id" ,item.getId());
        values.put("nombre_pokemon", item.getName());
        values.put("capturado",getDateTime());
        Log.d("AGREGAR_FAVORITO","VALORES"+values.toString());
        long id = db.insertWithOnConflict(TABLE_FAVS, null, values,SQLiteDatabase.CONFLICT_REPLACE);
        Log.d("AGREGAR_FAVORITO","Se trato de actualizar el ID>"+id);
//       -----------------------------------------------------------
//        -------IMPORTANTE----------------------------------------
//        --------------------------------------------
        db.close();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * ESTE METODO KJSDFHSLKJDAFLKJHASDLFKJAHSDFKLJSK.
     *
     * @return pokes Lista de pokemones traidos de SQLIte
     */
    public List<PokemonRespuesta.Pokemon> traerPokemones(){

        List<PokemonRespuesta.Pokemon> pokes= new LinkedList<>();

        String query="SELECT * FROM "+TABLE_FAVS;

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor= db.rawQuery(query, null);
        PokemonRespuesta.Pokemon pok=null;
        if (cursor.moveToFirst()){
            do{
                pok=new PokemonRespuesta.Pokemon();
                pok.setId(cursor.getInt(0));
                pok.setName(cursor.getString(1));
                pok.setUrl(Constantes.BASE_URL+Constantes.ENDPOINTS.POKEMONES+cursor.getInt(0));
                pokes.add(pok);
            } while (cursor.moveToNext());
        }

        Log.d("get_pokes", pokes.toString());
        db.close();
        return pokes;

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
  
    public void vaciarFavoritos() {
        SQLiteDatabase db=this.getWritableDatabase();
        String VACIAR_TABLA_FAVS = "DELETE FROM "+TABLE_FAVS+"";
        db.execSQL(VACIAR_TABLA_FAVS);
        String LIMPIEZA = "VACUUM";
        db.execSQL(LIMPIEZA);
        Log.d("SQLITE-DB","VACIADA LA TABLA "+TABLE_FAVS);
        db.close();
    }
}
