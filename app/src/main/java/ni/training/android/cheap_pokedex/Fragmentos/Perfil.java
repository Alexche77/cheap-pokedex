package ni.training.android.cheap_pokedex.Fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.view.ViewGroup;

import java.util.List;

import ni.training.android.cheap_pokedex.Models.PokemonRespuesta;
import ni.training.android.cheap_pokedex.R;
import ni.training.android.cheap_pokedex.Tools.MySQLiteOpenHelper;

public class Perfil extends Fragment {
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private MyPokemonRecyclerViewAdapter adaptadorPokemonesCapturados;
    private List<PokemonRespuesta.Pokemon> listaPokeCapturados;
    private LinearLayout seccionListaPokeCapturados;
    public static final String TAG = "FRAGMENTO PERFIL";
    private TextView pokesCapturados;
    private TextView mensajeAtrapalos;

    public Perfil() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static Perfil newInstance() {

        return new Perfil();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("onCreate","Trayendo lista!");
        listaPokeCapturados = actualizarLista();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_perfil, container, false);
        pokesCapturados = (TextView)view.findViewById(R.id.pokeCapturados);
        mensajeAtrapalos = (TextView)view.findViewById(R.id.bannerCapturalos);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        seccionListaPokeCapturados = (LinearLayout)view.findViewById(R.id.seccionCapturados);
//        Unimos el evento del boton liberar pokemones
        Button btnLiberarPokemones = (Button)view.findViewById(R.id.btn_librerar_pokemones);
        // Set the adapter
        Context context = view.getContext();
        int mColumnCount = 1;
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        btnLiberarPokemones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySQLiteOpenHelper helper = new MySQLiteOpenHelper(getContext());
                helper.vaciarFavoritos();
                listaPokeCapturados = actualizarLista();
                adaptadorPokemonesCapturados.notifyDataSetChanged();
                actualizarInterfaz();

            }
        });
        adaptadorPokemonesCapturados = new MyPokemonRecyclerViewAdapter(listaPokeCapturados,mListener);
        recyclerView.setAdapter(adaptadorPokemonesCapturados);
        actualizarInterfaz();
        return view;
    }

    private void actualizarInterfaz() {
        if(listaPokeCapturados.isEmpty()){
            seccionListaPokeCapturados.setVisibility(View.INVISIBLE);
            Log.d(TAG,"Ocultando seccion de pokemones capturados");
        }else{
            seccionListaPokeCapturados.setVisibility(View.VISIBLE);
            mensajeAtrapalos.setVisibility(View.VISIBLE);
            Log.d(TAG,"Revelando los botones y lista de pokemones capturados");
        }

        pokesCapturados.setText("Pokemones capturados: "+listaPokeCapturados.size());
    }

    private List<PokemonRespuesta.Pokemon> actualizarLista() {
//        Hacemos la busqueda de la DB
        List<PokemonRespuesta.Pokemon> pokex = null;
        try{
            MySQLiteOpenHelper helper = new MySQLiteOpenHelper(getContext());
            pokex= helper.traerPokemones();

        }catch (Exception e){
            Log.e("ERROR","Excepcion DB!");
        }
        Log.d("poke","LISTA ENVIADA");
        return pokex;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(PokemonRespuesta.Pokemon item);
    }


}
