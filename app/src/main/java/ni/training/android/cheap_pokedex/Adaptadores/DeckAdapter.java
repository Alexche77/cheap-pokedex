package ni.training.android.cheap_pokedex.Adaptadores;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;

import ni.training.android.cheap_pokedex.Models.PokemonRespuesta;
import ni.training.android.cheap_pokedex.R;
import ni.training.android.cheap_pokedex.Tools.Constantes;

/**
 * Created by Alvaro on 3/9/2017.
 */

public class DeckAdapter extends ArrayAdapter<PokemonRespuesta.Pokemon> {
    private final int mResource;

    public DeckAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull LinkedList<PokemonRespuesta.Pokemon> objects) {
        super(context, resource, objects);
        mResource = R.layout.card_pokemon;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View contentView, @NonNull ViewGroup parent) {
        final RelativeLayout layout;
        ViewHolder holder;
        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.card_pokemon, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }
        PokemonRespuesta.Pokemon item = getItem(position);
        if (item != null) {
            String nombre = "";
            /*Aqui tratamos de convertir el nombre a SentenceCase
            *
            * Clase makumba :v
            * */
            char letraMayus = item.getName().substring(0,1).toUpperCase().toCharArray()[0];
            char[] nombree = item.getName().toCharArray();
            for (int i = 0; i < nombree.length; i++) {
                if (i ==0)
                    nombre+=letraMayus;
                else
                    nombre+=nombree[i];
            }
            holder.nombre.setText(nombre);
            Picasso.with(getContext())
                    .load(Constantes.URL_IMG_POKEMONES_GRANDES+item.getId()+".png")
                    .error(R.drawable.web_hi_res_512)
                    .into(holder.imagen);
        }else{
            Log.d("DECKADAPTER","NO HAY POKEMON");
        }
        return contentView;
    }

    private static class ViewHolder{
        TextView nombre;
        ImageView imagen;

        public ViewHolder(View view){
            this.nombre = (TextView)view.findViewById(R.id.pokemonCardNombre);
            this.imagen = (ImageView)view.findViewById(R.id.spritePokemonCard);

        }
    }
}
